#ifndef __TFT_Z_H__
#define __TFT_Z_H__
#include "lm4f120h5qr.h"
#include "stdint.h"

void Write_Command(unsigned int Wcommand);
void Write_Data_OnBus(unsigned int data);
void TFT_Fill(unsigned int color);
void TFT_Set_Address(unsigned int PX1,unsigned int PY1,unsigned int PX2,unsigned int PY2);
void TFT_Init();
void Write_Data(unsigned int Wdata);
void Write_Command_Data(unsigned int Wcommand,unsigned int Wdata);

#endif // __TFT_Z_H__