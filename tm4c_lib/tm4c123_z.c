#include "stdint.h"
#include "lm4f120h5qr.h"
#include "tm4c123_z.h"

#define GPIOA_APB GPIO_PORTA_DATA_BITS_R
#define GPIOA_AHB GPIO_PORTA_AHB_DATA_BITS_R
#define GPIOB_APB GPIO_PORTB_DATA_BITS_R
#define GPIOB_AHB GPIO_PORTB_AHB_DATA_BITS_R
#define GPIOC_APB GPIO_PORTC_DATA_BITS_R
#define GPIOC_AHB GPIO_PORTC_AHB_DATA_BITS_R
#define GPIOD_APB GPIO_PORTD_DATA_BITS_R
#define GPIOD_AHB GPIO_PORTD_AHB_DATA_BITS_R
#define GPIOE_APB GPIO_PORTE_DATA_BITS_R
#define GPIOE_AHB GPIO_PORTE_AHB_DATA_BITS_R
#define GPIOF_APB GPIO_PORTF_DATA_BITS_R
#define GPIOF_AHB GPIO_PORTF_AHB_DATA_BITS_R

#define CPU_CLOCK 80000000

#define GPIODATA 0x3FC
#define GPIODIR 0x400
#define GPIOIS 0x404
#define GPIOIBE 0x408
#define GPIOIEV 0x40C
#define GPIOIM 0x410
#define GPIORIS 0x414
#define GPIOMIS 0x418
#define GPIOICR 0x41C
#define GPIOAFSEL 0x420
#define GPIODR2R 0x500
#define GPIODR4R 0x504
#define GPIODR8R 0x508
#define GPIOODR 0x50C
#define GPIOPUR 0x510
#define GPIOPDR 0x514
#define GPIOSLR 0x518
#define GPIODEN 0x51C
#define GPIOLOCK 0x520
#define GPIOCR 0x524
#define GPIOAMSEL 0x528
#define GPIOPCTL 0x52C
#define GPIOADCCTL 0x530
#define GPIODMACTL 0x534
#define GPIOPeriphID4 0xFD0
#define GPIOPeriphID5 0xFD4
#define GPIOPeriphID6 0xFD8
#define GPIOPeriphID7 0xFDC
#define GPIOPeriphID0 0xFE0
#define GPIOPeriphID1 0xFE4
#define GPIOPeriphID2 0xFE8
#define GPIOPeriphID3 0xFEC
#define GPIOPCellID0 0xFF0
#define GPIOPCellID1 0xFF4
#define GPIOPCellID2 0xFF8
#define GPIOPCellID3 0xFFC

static uint8_t PORTA = 0X00;
static uint8_t PORTB = 0X00;
static uint8_t PORTC = 0X00;
static uint8_t PORTD = 0X00;
static uint8_t PORTE = 0X00;
static uint8_t PORTF = 0X00;

/*


typedef struct
{
  volatile const unsigned long* GPIOA;
  const unsigned long* GPIOB;
  const unsigned long* GPIOC = GPIOC_APB;
  const unsigned long* GPIOD = GPIOD_APB;
  const unsigned long* GPIOE = GPIOE_APB;
  const unsigned long* GPIOF = GPIOF_APB;
  
} GPIO_APB;

typedef struct
{
  const unsigned long* GPIOA = (unsigned long)GPIOA_AHB;
  const unsigned long* GPIOB = GPIOB_AHB;
  const unsigned long* GPIOC = GPIOC_AHB;
  const unsigned long* GPIOD = GPIOD_AHB;
  const unsigned long* GPIOE = GPIOE_AHB;
  const unsigned long* GPIOF = GPIOF_AHB;
  
} GPIO_AHB;

*/

void set_GPIO_DOUT(volatile unsigned long* GPIOx_AxB, uint8_t data)
{
  if(GPIOx_AxB==GPIOA_APB||GPIOx_AxB==GPIOA_AHB) {
    SYSCTL_RCGCGPIO_R = 0x01;
    PORTA=data;
  }
  else if(GPIOx_AxB==GPIOB_APB||GPIOx_AxB==GPIOB_AHB) {
    SYSCTL_RCGCGPIO_R = 0x02;
    PORTB=data;
  }
  else if(GPIOx_AxB==GPIOC_APB||GPIOx_AxB==GPIOC_AHB) {
    SYSCTL_RCGCGPIO_R = 0x04;
    PORTC=data;
  }
  else if(GPIOx_AxB==GPIOD_APB||GPIOx_AxB==GPIOD_AHB) {
    SYSCTL_RCGCGPIO_R = 0x08;
    PORTD=data;
  }
  else if(GPIOx_AxB==GPIOE_APB||GPIOx_AxB==GPIOE_AHB) {
    SYSCTL_RCGCGPIO_R = 0x10;
    PORTE=data;
  }
  else if(GPIOx_AxB==GPIOF_APB||GPIOx_AxB==GPIOF_AHB) {
    SYSCTL_RCGCGPIO_R = 0x20;
    PORTF=data;
  }
  *(GPIOx_AxB+GPIODEN/4)=data;
  *(GPIOx_AxB+GPIODIR/4)=data;
  *(GPIOx_AxB+GPIODATA/4)=data;
}

uint8_t get_GPIO_DIN(volatile unsigned long* GPIOx_AxB, uint8_t msk)
{
  if(GPIOx_AxB==GPIOA_APB||GPIOx_AxB==GPIOA_AHB) SYSCTL_RCGCGPIO_R = 0x01;
  else if(GPIOx_AxB==GPIOB_APB||GPIOx_AxB==GPIOB_AHB) SYSCTL_RCGCGPIO_R = 0x02;
  else if(GPIOx_AxB==GPIOC_APB||GPIOx_AxB==GPIOC_AHB) SYSCTL_RCGCGPIO_R = 0x04;
  else if(GPIOx_AxB==GPIOD_APB||GPIOx_AxB==GPIOD_AHB) SYSCTL_RCGCGPIO_R = 0x08;
  else if(GPIOx_AxB==GPIOE_APB||GPIOx_AxB==GPIOE_AHB) SYSCTL_RCGCGPIO_R = 0x10;
  else if(GPIOx_AxB==GPIOF_APB||GPIOx_AxB==GPIOF_AHB) SYSCTL_RCGCGPIO_R = 0x20;
  *(GPIOx_AxB+GPIODEN/4)=msk;
  *(GPIOx_AxB+GPIODIR/4)=!msk;
  return *(GPIOx_AxB+GPIODATA/4);
}

void delay_ms(unsigned int delay)
{
NVIC_ST_init();
  for(unsigned int d=0;d<=delay;d++)
  {
  NVIC_ST_CURRENT_R=100000;
  unsigned int current_t=NVIC_ST_CURRENT_R;
  for(unsigned int time=0;time<=40000;time=(current_t-(unsigned int)NVIC_ST_CURRENT_R)) {}
  }
  
}

void NVIC_ST_init()
{
  
     NVIC_ST_CTRL_R=0;
  NVIC_ST_RELOAD_R=0x80000;
  NVIC_ST_CURRENT_R=0;
  NVIC_ST_CTRL_R=0x00000005;
}

void set_GPIO_POUT(volatile unsigned long* GPIOx_AxB, uint8_t pin, uint8_t p_state)
{
  uint8_t val = 0x00u + (1u << pin);
  if(GPIOx_AxB==GPIOA_APB||GPIOx_AxB==GPIOA_AHB) {
   // PORTA = *(GPIOx_AxB+GPIODATA/4);
    PORTA &= (~val);
    if(p_state==1) PORTA |= val;
    set_GPIO_DOUT(GPIOx_AxB, PORTA);
  }
  else if(GPIOx_AxB==GPIOB_APB||GPIOx_AxB==GPIOB_AHB){
   // PORTB = *(GPIOx_AxB+GPIODATA/4);
    PORTB &= (~val);
    if(p_state==1) PORTB |= val;
    set_GPIO_DOUT(GPIOx_AxB, PORTB);
  }
  else if(GPIOx_AxB==GPIOC_APB||GPIOx_AxB==GPIOC_AHB){
    //PORTC = *(GPIOx_AxB+GPIODATA/4);
    PORTC &= (~val);
    if(p_state==1) PORTC |= val;
    set_GPIO_DOUT(GPIOx_AxB, PORTC);
  }
  else if(GPIOx_AxB==GPIOD_APB||GPIOx_AxB==GPIOD_AHB) {
   // PORTD = *(GPIOx_AxB+GPIODATA/4);
    PORTD &= (~val);
    if(p_state==1) PORTD |= val;
    set_GPIO_DOUT(GPIOx_AxB, PORTD);
  }
  else if(GPIOx_AxB==GPIOE_APB||GPIOx_AxB==GPIOE_AHB) {
    //PORTE = *(GPIOx_AxB+GPIODATA/4);
    PORTE &= (~val);
    if(p_state==1) PORTE |= val;
    set_GPIO_DOUT(GPIOx_AxB, PORTE);
  }
  else if(GPIOx_AxB==GPIOF_APB||GPIOx_AxB==GPIOF_AHB) {
    //PORTF = *(GPIOx_AxB+GPIODATA/4);
    PORTF &= (~val);
    if(p_state==1) PORTF |= val;
    set_GPIO_DOUT(GPIOx_AxB, PORTF);
  }
  
}
