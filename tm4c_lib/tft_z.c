#include "tm4c123_z.h"
#include "tft_z.h"
#include "stdint.h"

#define RS0 set_GPIO_POUT(GPIOE_APB,1,0)
#define RS1 set_GPIO_POUT(GPIOE_APB,1,1)
#define WR0 set_GPIO_POUT(GPIOE_APB,2,0)
#define WR1 set_GPIO_POUT(GPIOE_APB,2,1)
#define CS0 set_GPIO_POUT(GPIOE_APB,3,0)
#define CS1 set_GPIO_POUT(GPIOE_APB,3,1)
#define REST0 set_GPIO_POUT(GPIOE_APB,4,0)
#define REST1 set_GPIO_POUT(GPIOE_APB,4,1)
#define WR 2
#define CS 3
#define REST 4

void Write_Command(unsigned int Wcommand)
{
  RS0;
  delay_ms(5);
  Write_Data_OnBus(Wcommand);
  WR0;
  delay_ms(5);
  WR1;
  /*
  TFT_RD = 1;
  TFT_RS = 0;
  TFT_DP_Hi = wcommand >> 8;
  TFT_DP_Lo = wcommand ;
  TFT_WR = 0;
  TFT_WR = 1;
  */
}

void Write_Data(unsigned int Wdata)
{
  RS1;
  delay_ms(5);
  Write_Data_OnBus(Wdata);
  WR0;
  delay_ms(5);
  WR1;
  /*
  TFT_RD = 1;
  TFT_RS = 1 ;
  TFT_DP_Hi = Wdata >>8 ;
  TFT_DP_Lo = wdata;
  TFT_WR = 0;
  TFT_WR = 1 ;
  */
}

void Write_Command_Data(unsigned int Wcommand,unsigned int Wdata)
{
   Write_Command(Wcommand);
   delay_ms(10);
   Write_Data(Wdata);
   delay_ms(10);
}

void Write_Data_OnBus(unsigned int data)
{
  set_GPIO_DOUT(GPIOA_APB, ((data&0x00F0u)>>2));
  //set_GPIO_DOUT(GPIOD_APB,0x0E);
  set_GPIO_DOUT(GPIOF_APB, data&(0x000F)); 
  
  set_GPIO_DOUT(GPIOB_APB, data>>8);  
}

void TFT_Init()
{
        //TFT_RD = 1;
        REST1;
        delay_ms(5);
        REST0;
        delay_ms(15);
        REST1;
        delay_ms(15);
        CS0;
        Write_Command_Data(0x0000,0x0001);
        Write_Command_Data(0x0003,0xA8A4);
        Write_Command_Data(0x000C,0x0000);
        Write_Command_Data(0x000D,0x800C);
        Write_Command_Data(0x000E,0x2B00);
        Write_Command_Data(0x001E,0x00B7);
        Write_Command_Data(0x0001,0x2B3F);
        Write_Command_Data(0x0002,0x0600);
        Write_Command_Data(0x0010,0x0000);
        Write_Command_Data(0x0011,0x6070);
        Write_Command_Data(0x0005,0x0000);
        Write_Command_Data(0x0006,0x0000);
        Write_Command_Data(0x0016,0xEF1C);
        Write_Command_Data(0x0017,0x0003);
        Write_Command_Data(0x0007,0x0233);
        Write_Command_Data(0x000B,0x0000);
        Write_Command_Data(0x000F,0x0000);
        Write_Command_Data(0x0041,0x0000);
        Write_Command_Data(0x0042,0x0000);
        Write_Command_Data(0x0048,0x0000);
        Write_Command_Data(0x0049,0x013F);
        Write_Command_Data(0x004A,0x0000);
        Write_Command_Data(0x004B,0x0000);
        Write_Command_Data(0x0044,0xEF95);
        Write_Command_Data(0x0045,0x0000);
        Write_Command_Data(0x0046,0x013F);
        Write_Command_Data(0x0030,0x0707);
        Write_Command_Data(0x0031,0x0204);
        Write_Command_Data(0x0032,0x0204);
        Write_Command_Data(0x0033,0x0502);
        Write_Command_Data(0x0034,0x0507);
        Write_Command_Data(0x0035,0x0204);
        Write_Command_Data(0x0036,0x0204);
        Write_Command_Data(0x0037,0x0502);
        Write_Command_Data(0x003A,0x0302);
        Write_Command_Data(0x003B,0x0302);
        Write_Command_Data(0x0023,0x0000);
        Write_Command_Data(0x0024,0x0000);
        Write_Command_Data(0x0025,0x8000);
        Write_Command_Data(0x004f,0x0000);
        Write_Command_Data(0x004e,0x0000);
        Write_Command(0x0022);
        CS1;
}

void TFT_Set_Address(unsigned int PX1,unsigned int PY1,unsigned int PX2,unsigned int PY2)
{
  //CS0;
  //delay_ms(5);
  Write_Command_Data(68,(PX2 << 8) + PX1 );  //Column address start2
  Write_Command_Data(69,PY1);      //Column address start1
  Write_Command_Data(70,PY2);  //Column address end2
  Write_Command_Data(78,PX1);      //Column address end1
  Write_Command_Data(79,PY1);  //Row address start2
  Write_Command(34);
  //CS1;
  //delay_ms(5);
}

/*
unsigned int Set_color(unsigned int r,unsigned int g,unsigned int b)
{
  unsigned int temp;
  Hi(temp) = (R & 0xF8) | (G >> 5);
              G = (G & 0x1C);
  Lo(temp) = (G << 3) | (B >>3);
  return temp;
}
*/

void TFT_Fill(unsigned int color)
{
  unsigned int i,j;
  CS0;
  TFT_Set_Address(0,0,239,319);
  Write_Data(color);
  for(i = 0; i <= 319; i++)
  {
    for(j = 0; j <= 239; j++)
    {
        WR0;
        WR1;
          delay_ms(1);
    }
  }
  CS1;
}
