#ifndef __TFT_Z_H__
#define __TFT_Z_H__
#include "lm4f120h5qr.h"
#include "stdint.h"

void LCD_Writ_Bus(char VH,char VL);
void LCD_Write_COM(char VH,char VL);
void LCD_init();

#endif // __TFT_Z_H__