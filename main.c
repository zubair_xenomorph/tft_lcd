#include "tm4c_lib/tm4c123_z.h"
#include "tm4c_lib/tft_z.h"
#include "tm4c_lib/lm4f120h5qr.h"
#include "stdint.h"

static unsigned int count=0;

static uint8_t value=0;

int main()
{
  set_GPIO_DOUT(GPIOD_APB,0x00);
  set_GPIO_DOUT(GPIOA_APB,0x00);
   GPIO_PORTA_LOCK_R=GPIO_LOCK_KEY;
   GPIO_PORTA_CR_R=0xFC;
   GPIO_PORTA_AFSEL_R &= ~0xFC;
   GPIO_PORTA_AMSEL_R&=~0xFC;
   
   set_GPIO_DOUT(GPIOF_APB,0x00);
   GPIO_PORTF_LOCK_R=GPIO_LOCK_KEY;
   GPIO_PORTF_CR_R=0x0F;
   GPIO_PORTF_AFSEL_R &= ~0x0F;
   GPIO_PORTF_AMSEL_R&=~0x0F;
   
   set_GPIO_DOUT(GPIOE_APB,0x00);
   GPIO_PORTE_LOCK_R=GPIO_LOCK_KEY;
   GPIO_PORTE_CR_R=0x3E;
   GPIO_PORTE_AFSEL_R &= ~0x3E;
   GPIO_PORTE_AMSEL_R&=~0x3E;
   
   set_GPIO_DOUT(GPIOB_APB,0x00);
   GPIO_PORTB_LOCK_R=GPIO_LOCK_KEY;
   GPIO_PORTB_CR_R=0xFF;
  GPIO_PORTB_AFSEL_R &= ~0xFF;
   GPIO_PORTB_AMSEL_R&=~0xFF;
   
    set_GPIO_DOUT(GPIOA_APB,0x00);
    set_GPIO_DOUT(GPIOB_APB,0x00);
    set_GPIO_DOUT(GPIOE_APB,0x00);
    set_GPIO_DOUT(GPIOF_APB,0x00);
    TFT_Init();
 
    

  while(1)
  {
   
    
   TFT_Fill(234);
   delay_ms(100);
   TFT_Fill(0);
   delay_ms(100);
   TFT_Fill(43433);
   delay_ms(100);
  }
  GPIO_PORTD_LOCK_R=0;
  GPIO_PORTF_LOCK_R=0;
  GPIO_PORTE_LOCK_R=0;
  GPIO_PORTB_LOCK_R=0;
  
  
  return 0;
}
